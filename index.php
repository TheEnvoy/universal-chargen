<?php

error_reporting(E_ALL^E_NOTICE);
require 'vendor/autoload.php';

function is_assoc($array) {
	return (bool)count(array_filter(array_keys($array), 'is_string'));
}

$system = empty( $_GET['system']) ? 'example' : 'system';
$stats = json_decode(file_get_contents('systems/'.$system.'/stats.json'), 1);

$loader = new Twig_Loader_Filesystem('template');
$twig = new Twig_Environment($loader, array());

if( empty( $_GET )) {
	$template = $twig->loadTemplate('create.twig');
	echo $template->render(['stats' => $stats]);
} else {
	$char = array();

	foreach( $stats as $gid => $group ) {
		if( is_assoc( $_GET[$gid] )) {
			$char[$gid] = $_GET[$gid];
		} else {
			$char[$gid] = array();
			foreach( $_GET[$gid] as $skill )
				$char[$gid][$skill['type']] = $skill['value'];
		}
	}

	$template = $twig->loadTemplate('character.twig');
	echo $template->render(['char' => $char]);
}
